/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {Button, StyleSheet, TextInput, View} from 'react-native';
import * as DB from "./src/DbManager";
import {ProgressDialog} from 'react-native-simple-dialogs';
import * as Constants from "./src/Constants";


type Props = {};
let self;
export default class App extends Component<Props> {

    constructor() {
        super();
        this.fetch = this.fetch.bind(this);
        this.state = {
            msg: "",
            result: "",
            progressVisible: false
        };
        self = this;
    }

    render() {
        return (
            <View style={styles.container}>

                <ProgressDialog
                    visible={this.state.progressVisible}
                    title="Data Syncing"
                    message={this.state.msg}
                />
                <Button
                    onPress={() => this.fetch()}
                    title="Fetch"
                    color="#841584"
                />
                <TextInput defaultValue={JSON.stringify(this.state.result, null, 2)}
                           multiline={true}/>
            </View>
        );
    }

    componentDidMount() {
        console.log("stocks count: " + DB.Ops.getStocks().length);
        console.log("cities count: " + DB.Ops.getCities().length);
        let makes = DB.Ops.getMakes(Constants.MAKE_NAME + " BEGINSWITH 'H'", Constants.MAKE_NAME);
        console.log("Make count: ", makes.length, JSON.stringify(makes));
        console.log("Model count: " + DB.Ops.getModels().length);
        console.log("Version count: " + DB.Ops.getVersions().length);
    }

    fetch() {

        let handler = {
            publish: function (message) {
                self.setState({msg: message});
            },
            onComplete: function (config) {

                let response = "{stocks count: " + DB.Ops.getStocks().length + "," +
                    "cities count: " + DB.Ops.getCities().length + "," +
                    "make count: " + DB.Ops.getMakes().length + "," +
                    "models count: " + DB.Ops.getModels().length + "," +
                    "versions count: " + DB.Ops.getVersions().length + "}";

                self.setState({result: response, progressVisible: false});
                console.log(JSON.stringify(config, null, 2));
            }
        };

        let cityConfig = DB.prepareCityConfig("http://beta.priceindex.usedcarsin.in/api/stock/v1/getup", "POST", {
            method: "city",
            apikey: "U3KqyrewdMuCotTS"
        });

        let mmvConfig = DB.prepareMmvConfig("http://beta.priceindex.usedcarsin.in/api/ins/v1/getup", "POST", {
            method: "all_make",
            apikey: "U3KqyrewdMuCotTS"
        });

        let stockConfig = DB.prepareStockConfig("http://usedcarsin.in/api/ins/v1/getup", "POST", {
            changetime: `2018-04-28+13%3A05%3A08`,
            pageNumber: 1,
            access_code: `4ab9bcd0cebad9991429fdc516c5f4d5`,
            cloud_owner: `GAADI`,
            ANDROID_ID: `20ca09584e710a0f`,
            source: `ANDROID_APP`,
            android_version: `8.0.0 N_MR1`,
            APP_VERSION: 65,
            output: `json`,
            device_name: `Google Android SDK built for x86`,
            packageName: `com.gcloud.gaadi`,
            network_provider: `Android`,
            SERVICE_EXECUTIVE_LOGIN: false,
            user_email: `saroj.sahoo@gaadi.com`,
            method: `getCommonStockList`,
            apikey: `U3KqyrewdMuCotTS`,
            dd_user_id: 15,
            ipAddress: `192.168.232.2`,
            network_name: `WIFI`,
            ucdid: 1217,
            rpp: 100,
            userType: 0,
            UC_DEALER_USERNAME: `saroj.sahoo@gaadi.com`,
            username: `saroj.sahoo@gaadi.com`
        });

        self.setState({progressVisible: true});

        /* Individual Calls */
        // DB.initializeCities(cityConfig, handler);
        // DB.initializeMmv(mmvConfig, handler);

        /* Combined initialisation*/
        DB.Ops.dropStocks();
        let combinedConfig = {
            ...cityConfig,
            ...mmvConfig,
            ...stockConfig
        };
        DB.init(combinedConfig, handler);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
