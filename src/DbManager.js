import * as DBFunctions from "./DBFunction";
import {Calls} from "./api";
import * as Util from "./Util";


const Ops = {
    dropCities: DBFunctions.dropCities,
    getCities: DBFunctions.getCities,
    dropMakes: DBFunctions.dropMakes,
    getMakes: DBFunctions.getMakes,
    dropModels: DBFunctions.dropModels,
    getModels: DBFunctions.getModels,
    dropVersions: DBFunctions.dropVersions,
    getVersions: DBFunctions.getVersions,
    dropStocks: DBFunctions.dropStocks,
    getStocks: DBFunctions.getStocks
};

export {
    Ops,
    init,
    initializeCities,
    initializeMmv,
    initializeStock,
    prepareCityConfig,
    prepareMmvConfig,
    prepareStockConfig
};


/**
 * @param config  Object def {  ...prepareCityConfig(), ...prepareMmvConfig(), ... }
 * @param messageHandler  Object def {  publish: function(message){}   }
 */
function init(config, messageHandler = {
    publish: function (message) {
    },
    onComplete: function (result) {
    }
}) {

    // make copy of received config for updating and returning response
    let receivedConfig = JSON.parse(JSON.stringify(config));
    // Util.l(JSON.stringify(receivedConfig));
    let generatedConfig = {};

    let localMsgHandler = {
        publish: function (message) {
            messageHandler.publish(message);
        },
        onComplete: function (result) {
            generatedConfig = {...generatedConfig, ...JSON.parse(JSON.stringify(result))};
            check();
        }
    };

    // function for monitoring the overall syncs in progress & initiating onComplete callback
    let check = function () {
        let goAhead = true;
        for (let attr in receivedConfig) {
            // check that each object, received in config for syncing, has been assigned a response object!
            if (receivedConfig.hasOwnProperty(attr))
                if (!generatedConfig[attr] || generatedConfig[attr].response === undefined)
                    goAhead = false;
        }

        // if all the responses have been collected, then perform the callback
        if (goAhead) {
            Util.l("Config: ", JSON.stringify(generatedConfig));
            messageHandler.publish("Complete");
            messageHandler.onComplete(generatedConfig);
        }
    };

    // iterate through config properties to start respective sync processes
    for (let attr in config) {
        if (config.hasOwnProperty(attr) && attr === "city") {
            this.initializeCities({[attr]: config[attr]}, localMsgHandler);
        } else if (config.hasOwnProperty(attr) && attr === "mmv") {
            this.initializeMmv({[attr]: config[attr]}, localMsgHandler);
        } else if (config.hasOwnProperty(attr) && attr === "stocks") {
            this.initializeStock({[attr]: config[attr]}, localMsgHandler);
        }
    }
}


/**
 *
 * @param url  server Url
 * @param method  POST/GET/PUT ...
 * @param param  Form Data Parameters eg. {method : "xyz"}
 * @returns {{city: {url: *, method: *, param: *}}}
 */
function prepareCityConfig(url, method, param) {
    return {city: {url, method, param}};
}

/**
 *
 * @param url  server Url
 * @param method  POST/GET/PUT ...
 * @param param  Form Data Parameters eg. {method : "xyz"}
 * @returns {{mmv: {url: *, method: *, param: *}}}
 */
function prepareMmvConfig(url, method, param) {
    return {mmv: {url, method, param}};
}

/**
 *
 * @param url  server Url
 * @param method  POST/GET/PUT ...
 * @param param  Form Data Parameters eg. {method : "xyz"}
 * @returns {{mmv: {url: *, method: *, param: *}}}
 */
function prepareStockConfig(url, method, param) {
    return {stocks: {url, method, param}};
}

/**
 *
 * @param config  DB.prepareXyzConfig('','',{})
 * @param messageHandler
 */
function initializeCities(config, messageHandler = {
    publish: function (message) {
        Util.l(message);
    },
    onComplete: function () {
    }
}) {
    // This callback is forwarded to API call and success or failure are called based on API response
    let callback = {
        success: async function (respJson = {cityList: []}) {
            messageHandler.publish("Syncing Cities ..");

            DBFunctions.dropCities();
            let dbResponse = await DBFunctions.insertCities(respJson.cityList);
            messageHandler.publish("Syncing Cities ...");
            let output = JSON.parse(JSON.stringify(config));
            output.city.response = {success: dbResponse, error: undefined};
            messageHandler.onComplete(output);

        },
        failure: function (error) {
            messageHandler.publish("Failed Syncing City !");
            let output = JSON.parse(JSON.stringify(config));
            output.city.response = {success: undefined, error: error};
            messageHandler.onComplete(output);

        }
    };
    messageHandler.publish("Syncing Cities .");
    Calls.getData(callback, config.city.url, config.city.method, config.city.param);
}

/**
 *
 * @param config  DB.prepareXyzConfig('','',{})
 * @param messageHandler
 */
function initializeMmv(config, messageHandler = {
    publish: function (message) {
        Util.l(message);
    },
    onComplete: function () {
    }
}) {
    let callback = {
        success: async function (respJson = {make: [], model: [], version: []}) {
            messageHandler.publish("Syncing MMV ..");
            let responseMake, responseModel, responseVersion;
            DBFunctions.dropMakes();
            responseMake = await DBFunctions.insertMake(respJson.make !== undefined ? respJson.make : []);
            DBFunctions.dropModels();
            responseModel = await DBFunctions.insertModel(respJson.model !== undefined ? respJson.model : []);
            DBFunctions.dropVersions();
            responseVersion = await DBFunctions.insertVersion(respJson.version !== undefined ? respJson.version : []);

            messageHandler.publish("Syncing MMV ...");
            let output = JSON.parse(JSON.stringify(config));
            output.mmv.response = {
                success: {
                    "make": {...responseMake},
                    "model": {...responseModel},
                    "version": {...responseVersion}
                }, error: undefined
            };
            messageHandler.onComplete(output);

        },
        failure: function (error) {
            messageHandler.publish("MMV Sync Failed!");
            let output = JSON.parse(JSON.stringify(config));
            output.mmv.response = {success: undefined, error: error};
            messageHandler.onComplete(output);
        }
    };
    messageHandler.publish("Syncing MMV .");
    Calls.getData(callback, config.mmv.url, config.mmv.method, config.mmv.param);
}

/**
 *
 * @param config  DB.prepareXyzConfig('','',{})
 * @param messageHandler
 */
function initializeStock(config, messageHandler = {
    publish: function (message) {
        Util.l(message);
    },
    onComplete: function () {
    }
}) {

    let callback = {
        success: async function (respJson) {

            let RPP = config.stocks.param.rpp && parseInt(config.stocks.param.rpp) || 50;
            let hasNext = false;
            messageHandler.publish("Syncing Stocks ..");

            if (respJson.status !== "T" ||
                respJson.stockData === undefined ||
                respJson.stockData.length === 0 ||
                respJson.stockData.length < RPP)
                hasNext = false;
            else if (respJson.stockData.length >= RPP) {
                hasNext = true;
            }

            let response = await DBFunctions.insertStocks(respJson.stockData);
            if (!hasNext) {
                messageHandler.publish("Syncing Stocks ...");
                let output = JSON.parse(JSON.stringify(config));
                output.stocks.response = {
                    success: response, error: undefined
                };
                messageHandler.onComplete(output);
            } else {
                messageHandler.publish("Syncing Stocks .");
                config.stocks && config.stocks.param.pageNumber && (config.stocks.param.pageNumber += 1);
                Calls.getData(callback, config.stocks.url, config.stocks.method, config.stocks.param);
            }
        },
        failure: function (error) {
            messageHandler.publish("Stock Sync Failed!");
            let output = JSON.parse(JSON.stringify(config));
            output.stocks.response = {success: undefined, error: error};
            messageHandler.onComplete(output);
        }
    };
    messageHandler.publish("Syncing Stocks .");
    Calls.getData(callback, config.stocks.url, config.stocks.method, config.stocks.param);
}
