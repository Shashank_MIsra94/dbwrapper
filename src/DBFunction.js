import {City, Dealer, Leads, Make, Model, Showroom, Stock, Version} from "./schema";
import * as db from "./Constants";
import * as Util from "./Util";

const cityRealm = new Realm({
    path: 'cityRealm.realm',
    schema: [City],
    schemaVersion: db.TABLE_CITY_SCHEMA_VERSION
});
const makeRealm = new Realm({
    path: 'makeRealm.realm',
    schema: [Make],
    schemaVersion: db.TABLE_MAKE_SCHEMA_VERSION
});
const modelRealm = new Realm({
    path: 'modelRealm.realm',
    schema: [Model],
    schemaVersion: db.TABLE_MODEL_SCHEMA_VERSION
});
const versionRealm = new Realm({
    path: 'versionRealm.realm',
    schema: [Version],
    schemaVersion: db.TABLE_VERSION_SCHEMA_VERSION
});
const leadRealm = new Realm({
    path: 'leadRealm.realm',
    schema: [Leads],
    schemaVersion: db.TABLE_LEADS_SCHEMA_VERSION
});
const stockRealm = new Realm({
    path: 'stockRealm.realm',
    schema: [Stock],
    schemaVersion: db.TABLE_STOCKS_SCHEMA_VERSION
});
const dealerRealm = new Realm({
    path: 'dealerRealm.realm',
    schema: [Dealer],
    schemaVersion: db.TABLE_DEALER_SCHEMA_VERSION
});
const showroomRealm = new Realm({
    path: 'showroomRealm.realm',
    schema: [Showroom],
    schemaVersion: db.TABLE_SHOWROOM_SCHEMA_VERSION
});

/**
 *
 * @param filterQuery
 * @param sort
 * @returns {any}
 */
export function getCities(filterQuery, sort) {
    let objects = cityRealm.objects(db.TABLE_CITY);
    if (filterQuery)
        objects = objects.filtered(filterQuery);
    if (sort)
        objects = objects.sorted(sort);
    return objects;
}

export function dropCities(cities) {
    if (!cities)
        cities = cityRealm.objects(db.TABLE_CITY);
    if (cities && cities.length > 0)
        cityRealm.write(() => {
            cityRealm.delete(cities);
        });
}

/**
 *
 * @param makeList
 * @returns {{insertCount: number, notInserted: Array}}
 */
export function insertMake(makeList) {
    let insertCount = 0, notInserted = [];
    if (makeList instanceof Array && makeList.length > 0) {
        makeRealm.write(() => {
            makeList.forEach((make) => {
                try {
                    makeRealm.create(db.TABLE_MAKE, {
                        [db.MAKE_ID]: make.m_id,
                        [db.MAKE_CENTRAL_ID]: make.cmk_id,
                        [db.MAKE_NAME]: make.mk
                    });
                    insertCount++;
                } catch (error) {
                    Util.l(error);
                    notInserted.push(make);
                }
            });
        })
    }
    return {insertCount, notInserted};
}

/**
 *
 * @param filterQuery
 * @param sort
 * @returns {any}
 */
export function getMakes(filterQuery, sort) {
    let objects = makeRealm.objects(db.TABLE_MAKE);
    if (filterQuery)
        objects = objects.filtered(filterQuery);
    if (sort)
        objects = objects.sorted(sort);
    return objects;
}

export function dropMakes(makes) {
    if (!makes)
        makes = makeRealm.objects(db.TABLE_MAKE);
    if (makes && makes.length > 0)
        makeRealm.write(() => {
            makeRealm.delete(makes);
        });
}

/**
 *
 * @param modelList
 * @returns {{insertCount: number, notInserted: Array}}
 */
export async function insertModel(modelList) {
    let insertCount = 0, notInserted = [];
    if (modelList instanceof Array && modelList.length > 0) {
        modelRealm.write(() => {
            modelList.forEach((model) => {
                try {
                    modelRealm.create(db.TABLE_MODEL, {
                        [db.MODEL_ID]: model.md_id,
                        [db.MODEL_CENTRAL_ID]: model.cm_id,
                        [db.MODEL_NAME]: model['model'],
                        [db.MODEL_PARENT_ID]: model.p_m_id,
                        [db.MODEL_RANK]: model.rank,
                        [db.MODEL_MAKE_NAME]: model.m_n,
                        [db.MODEL_MAKE_ID]: model.m_id,
                        [db.MODEL_BODY_TYPE]: model.b_t,
                        [db.MODEL_MMV_COLUMN_MAKE_MODEL]: Util.merge(model.m_n, model.model)
                    });
                    insertCount++;
                } catch (error) {
                    Util.l(error);
                    notInserted.push(model);
                }
            });
        })
    }
    return {insertCount, notInserted};
}

/**
 *
 * @param filterQuery
 * @param sort
 * @returns {any}
 */
export function getModels(filterQuery, sort) {
    let objects = modelRealm.objects(db.TABLE_MODEL);
    if (filterQuery)
        objects = objects.filtered(filterQuery);
    if (sort)
        objects = objects.sorted(sort);
    return objects;
}

export function dropModels(models) {
    if (!models)
        models = modelRealm.objects(db.TABLE_MODEL);
    if (models && models.length > 0)
        modelRealm.write(() => {
            modelRealm.delete(models);
        });
}

/**
 *
 * @param versionList
 * @returns {{insertCount: number, notInserted: Array}}
 */
export async function insertVersion(versionList) {
    let insertCount = 0, notInserted = [];
    if (versionList instanceof Array && versionList.length > 0) {
        versionRealm.write(() => {
            versionList.forEach((version) => {
                try {
                    versionRealm.create(db.TABLE_VERSION, {
                        [db.VERSION_ID]: version.v_id,
                        [db.VERSION_CENTRAL_ID]: version.cv_id,
                        [db.VERSION_NAME]: version.version,
                        [db.VERSION_MAKE_ID]: version.m_id,
                        [db.VERSION_MAKE_NAME]: version.mk,
                        [db.VERSION_MODEL_ID]: version.md_id,
                        [db.VERSION_MODEL_NAME]: version.md,
                        [db.VERSION_MMV_MAKE_MODEL]: Util.merge(version.mk, version.md),
                        [db.VERSION_MMV_MODEL_VERSION]: Util.merge(version.md, version.version),
                        [db.VERSION_MMV_MAKE_MODEL_VERSION]: Util.merge(version.mk, version.md, version.version),
                        [db.VERSION_MODEL_LAUNCH_YEAR]: version.s_y,
                        [db.VERSION_MODEL_END_YEAR]: version.e_y,
                        [db.VERSION_TRANSMISSION_TYPE]: version.t_t,
                        [db.VERSION_FUEL_TYPE]: version.f_t,
                        [db.VERSION_CC]: version.v_cc,
                    });
                    insertCount++;
                } catch (error) {
                    Util.l(error);
                    notInserted.push(version)
                }
            });
        })
    }
    return {insertCount, notInserted};
}

/**
 *
 * @param filterQuery
 * @param sort
 * @returns {any}
 */
export function getVersions(filterQuery, sort) {
    let objects = versionRealm.objects(db.TABLE_VERSION);
    if (filterQuery)
        objects = objects.filtered(filterQuery);
    if (sort)
        objects = objects.sorted(sort);
    return objects;
}

export function dropVersions(versions) {
    if (!versions)
        versions = versionRealm.objects(db.TABLE_VERSION);
    if (versions && versions.length > 0)
        versionRealm.write(() => {
            versionRealm.delete(versions);
        });
}

/**
 *
 * @param stocks
 * @returns {{insertCount: number, notInserted: Array}}
 */
export async function insertStocks(stocks) {
    let insertCount = 0, notInserted = [];
    if (stocks instanceof Array && stocks.length > 0) {
        stockRealm.write(() => {
            stocks.forEach((stock, index) => {
                try {
                    stockRealm.create(db.TABLE_STOCKS, {
                        [db.STOCK_ID]: stock.id,
                        [db.STOCK_DEALER_PLATFORM]: stock.d_p,
                        [db.STOCK_FINACNE_LIST]: stock.f_l,
                        [db.STOCK_CHANGE_TIME]: stock.changetime,
                        [db.STOCK_CAR_CERTIFICATION]: stock.c_cert,
                        [db.STOCK_USER_NAME]: stock.u_name,
                        [db.STOCK_MAKE_NAME]: stock.m_n,
                        [db.STOCK_MODEL_NAME]: stock.md_n,
                        [db.STOCK_TRANSMISSION]: stock.tm,
                        [db.STOCK_VERSION_NAME]: stock.v_n,
                        [db.STOCK_MAKE]: stock.mk,
                        [db.STOCK_BODY_TYPE]: stock.b_type,
                        [db.STOCK_OFFER_COUNT]: stock.offer_cnt,
                        [db.STOCK_MODEL_VERSION]: stock.md_v,
                        [db.STOCK_KMS]: stock.km,
                        [db.STOCK_SHOWROOMID]: stock.sroom_id,
                        [db.STOCK_PRICE]: stock.price,
                        [db.STOCK_RENEW_COUNT]: stock.renew_count,
                        [db.STOCK_COLOR]: stock.color,
                        [db.STOCK_MM]: stock.mm,
                        [db.STOCK_PRICE_SORT]: parseInt(stock.price_int),
                        [db.STOCK_KM_SORT]: parseInt(stock.km_int),
                        [db.STOCK_FUEL_TYPE]: stock.fuel,
                        [db.STOCK_REG_NO]: stock.regno,
                        [db.STOCK_MODEL_YEAR]: stock.m_y,
                        [db.STOCK_MOBILE]: stock.mno,
                        [db.STOCK_URL]: stock.URL,
                        [db.STOCK_EMAIL]: stock.email,
                        [db.STOCK_TRUST_MARK_CERTIFY]: stock.trustmark,
                        [db.STOCK_SHOWROOMID]: stock.sroom_id,
                        [db.STOCK_GAADIID]: stock.g_id,
                        [db.STOCK_CREATE_DATE]: stock.cr_date,
                        [db.STOCK_DOMAIN]: stock.dm,
                        [db.STOCK_CAR_ID]: stock.id,
                        [db.STOCK_HEX_CODE]: stock.h_c,
                        [db.STOCK_ACTIVE]: stock.active,
                        [db.STOCK_SHARE_TEXT]: prepareShareText(stock.m_n, stock.md_n, stock.fuel, stock.m_y, stock.color, stock.km, stock.price, stock.dm, stock.g_id),
                        [db.STOCK_TOTAL_LEADS]: stock.leads,
                        [db.STOCK_DEALER_PRICE]: stock.d_price,
                        [db.STOCK_TOTAL_LEADS]: stock.leads,
                        [db.STOCK_IMAGE_ICON]: stock.i_icon,
                        [db.STOCK_AREA_OF_COVER]: stock.a_o_c,
                        [db.STOCK_INSPECTED_CAR]: stock.insp_car,
                        [db.STOCK_INSPECTION_EXPIRY_DATE]: stock.insp_ex_d,
                        [db.STOCK_CNG_LPG_ENDORSEMENT]: stock.cnglpg,
                        [db.STOCK_FLIPMENT]: stock.flip,
                        [db.STOCK_REG_MONTH]: String(stock.reg_m),
                        [db.STOCK_REG_DAY]: String(stock.reg_d),
                        [db.STOCK_REG_YEAR]: String(stock.reg_y),
                        [db.STOCK_BOOKED]: stock.booked ? "1" : "0",
                        [db.STOCK_OWNER_TYPE]: stock.owner,
                        [db.STOCK_RC_URL]: stock.rc_u,
                        [db.STOCK_RC_URL_2]: stock.rc_u2,
                        [db.STOCK_IS_RSA]: stock.is_rsa,
                        [db.STOCK_MAKE_ID]: stock.m_id,
                        [db.STOCK_MODEL_ID]: stock.md_id,
                        [db.STOCK_VERSION_ID]: stock.v_id,
                        [db.STOCK_MMONTH]: stock.m_th,
                        [db.STOCK_EMI_FINANCE]: stock.e_f,
                        [db.STOCK_PRICE_EDIT_MESSAGE]: stock.price_edit_message,
                        [db.STOCK_IMAGES_ARRAY]: stock.img,
                        [db.STOCK_ENGINE_NUM]: stock.eng_num,
                        [db.STOCK_CHAHSIS_NUM]: stock.cha_num,
                        [db.STOCK_REG_MONTH]: stock.reg_m,
                        [db.STOCK_PARENT_MODEL_NAME]: stock.p_md_n,
                        [db.STOCK_IS_CLASSIFIED]: stock.isclassified ? 1 : 0,
                        [db.STOCK_IS_INSPECTED]: stock.is_insp ? 1 : 0,
                        [db.STOCK_IS_FEATURED]: stock.is_featured ? 1 : 0,
                        [db.STOCK_UPDATE_TIME]: stock.updateTime ? stock.updateTime : new Date().getTime() + "",
                    });
                    insertCount++;
                } catch (error) {
                    Util.l(error);
                    notInserted.push(stock)
                }
            });
        })
    }
    return {insertCount, notInserted};
}

/**
 *
 * @param filterQuery
 * @param sort
 * @returns {any}
 */
export function getStocks(filterQuery, sort) {
    let objects = stockRealm.objects(db.TABLE_STOCKS);
    if (filterQuery)
        objects = objects.filtered(filterQuery);
    if (sort)
        objects = objects.sorted(sort);

    return objects;
}

export function dropStocks(stocks) {
    if (!stocks)
        stocks = stockRealm.objects(db.TABLE_STOCKS);
    if (stocks && stocks.length > 0)
        stockRealm.write(() => {
            stockRealm.delete(stocks);
        });
}

function prepareShareText(make, model, fuel, modelYear, color, kms, price, domain, gaadiId) {
    return "Dear Customer, Please have a look at " +
        (make && make.length > 0 ? make : "") +
        (model && model.length > 0 ? (" " + model) : "") +
        (fuel && model.length > 0 ? (" " + fuel) : "") +
        (modelYear && modelYear.length > 0 ? (", " + modelYear + " Model") : "") +
        (color && color.length > 0 ? (", " + color + " Color") : "") +
        (kms && kms.length > 0 ? (", " + kms + " Kms Driven") : "") +
        (price && price.length > 0 ? (" @ Rs " + price) : "") +
        (domain && domain.length > 0 && gaadiId && gaadiId.length > 0 ?
            (" - http://" + domain + "/uc/" + gaadiId) : "");
}

/**
 *
 * @param cityList
 * @returns {{insertCount: number, notInserted: Array}}
 */
export async function insertCities(cityList) {
    let insertCount = 0, notInserted = [];
    if (cityList instanceof Array && cityList.length > 0) {
        cityRealm.write(() => {
            cityList.forEach((city) => {
                try {
                    cityRealm.create(db.TABLE_CITY, {
                        [db.CITY_ID]: city.city_id,
                        [db.CITY_CENTRAL_ID]: city.central_city_id,
                        [db.CITY_NAME]: city.city_name,
                        [db.CITY_STATE_ID]: city.state_id,
                        [db.CITY_STATE_NAME]: city.state_name,
                        [db.CITY_REGION_ID]: city.region_id,
                        [db.CITY_ORDER_BY]: city.order_by
                    });
                    insertCount++;
                } catch (error) {
                    Util.l(error);
                    notInserted.push(city);
                }
            });
        });
    }
    return {insertCount, notInserted};

}