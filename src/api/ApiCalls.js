import * as Utils from "../Util";


function getData(callback, url, method, param) {
    try {
        fetch(url, {
            method: method,
            body: Utils.paramsToBody(param)
        }).then((response) => {
            let resJson;
            try {
                resJson = response.json();
            } catch (e) {
                resJson = '';
                callback && callback.failure && callback.failure(e);
            }
            return resJson;
        }).then((respJson) => {
            if (!respJson)
                return;
            if (respJson.status !== "T" && callback && callback.failure) {
                Utils.l("API CALL FAILURE");
                callback.failure(respJson);
            } else {
                Utils.l("API CALL SUCCESS");
                callback.success(respJson).then(() => {
                });
            }

        }).catch((error) => {
            Utils.l(error);
            callback && callback.failure && callback.failure(error);
        });
    } catch (error) {
        callback && callback.failure && callback.failure({message: "No Internet Connection!"});
    }
}

export {getData};