import * as db from "../Constants";

export class Leads {

    static sequenceTitle = "seqTitle";
    static sequenceValue = 0;

    static COLUMN_ID_GENERATOR: {
        schema:{
            name:"leadColumnIdGenerator",
            primaryKey: Leads.sequenceTitle,
            properties:{
                [Leads.sequenceTitle]:'string',
                [Leads.sequenceValue]:{type:'int', default:0}
            }
        }
    };
}

Leads.schema = {
    name: db.TABLE_LEADS,
    primaryKey: db.LEAD_ID,
    properties: {
        [db.LEAD_ID]: 'int',
        [db.LEAD_COLUMN_ID]:'int',
        [db.LEAD_TYPE]:'string',
        [db.LEAD_DEALER_ID]:'int',
        [db.LEAD_RATING]:{type:'string', optional:true},
        [db.LEAD_FOLLOW_DATE_ANDROID]:{type:'string', optional:true},
        [db.LEAD_CHANGE_TIME]:'string',
        [db.LEAD_WALKIN_DATE]:{type:'string', optional:true},
        [db.LEAD_REMINDER_DATE]:{type:'string', optional:true},
        [db.LEAD_CREATED_ON]:{type:'string', optional:true},
        [db.LEAD_CURRENT_TIMESTAMP]:{type:'string', optional:true},
        [db.LEAD_REQUEST_CODE]:{type:'int', optional:true},
        [db.LEAD_NOTIFICATION_TIME]:{type:'string', optional:true},
        [db.LEAD_MAKE]:{type:'string', optional:true},
        [db.LEAD_MAKE_ID]:{type:'string', optional:true},
        [db.LEAD_MODEL]:{type:'string', optional:true},
        [db.LEAD_COLUMN_VERSION]:{type:'string', optional:true},
        [db.LEAD_LMS_STATUS]:{type:'string', optional:true},
        [db.LEAD_DATETIME]:'string',
        [db.LEAD_NAME]:{type:'string', optional:true},
        [db.LEAD_NUMBER]:'string',
        [db.LEAD_BUDGET]:{type:'string', optional:true},
        [db.LEAD_SOURCE]:{type:'string', optional:true},
        [db.LEAD_STATUS]:{type:'string', optional:true},
        [db.LEAD_VERIFIED]:{type:'string', optional:true},
        [db.LEAD_JSON_FORMAT]:'string'
    }
};
