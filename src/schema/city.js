import * as db from "../Constants";

export class City {}

City.schema = {
    name: db.TABLE_CITY,
    primaryKey: db.CITY_ID,
    properties: {
        [db.CITY_ID]: 'string',
        [db.CITY_CENTRAL_ID]: 'string',
        [db.CITY_NAME]:'string',
        [db.CITY_ORDER_BY]: 'string',
        [db.CITY_REGION_ID]:'string',
        [db.CITY_STATE_ID]:'string',
        [db.CITY_STATE_NAME]:'string',
    }
};