import * as db from "../Constants";

export class Version {}

Version.schema = {
    name: db.TABLE_VERSION,
    primaryKey: db.VERSION_ID,
    properties: {
        [db.VERSION_ID]: 'string',
        [db.VERSION_CENTRAL_ID]: {type: 'string', optional: true},
        [db.VERSION_NAME]: 'string',
        [db.VERSION_CC]: 'string',
        [db.VERSION_MAKE_ID]: 'string',
        [db.VERSION_MAKE_NAME]: 'string',
        [db.VERSION_MODEL_ID]: 'string',
        [db.VERSION_MODEL_NAME]: 'string',
        [db.VERSION_MODEL_LAUNCH_YEAR]: 'string',
        [db.VERSION_MODEL_END_YEAR]: 'string',
        [db.VERSION_FUEL_TYPE]: 'string',
        [db.VERSION_TRANSMISSION_TYPE]: 'string',
        [db.VERSION_MMV_MAKE_MODEL]: 'string',
        [db.VERSION_MMV_MODEL_VERSION]: 'string',
        [db.VERSION_MMV_MAKE_MODEL_VERSION]: 'string',
    }
};