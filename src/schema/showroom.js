import * as db from "../Constants";

export class Showroom {}
Showroom.schema = {
    name:db.TABLE_SHOWROOM,
    primaryKey:db.SHOWROOM_ID,
    properties :{
        [db.SHOWROOM_ID]:'string',
        [db.SHOWROOM_NAME]:'string',
        [db.SHOWROOM_DEALER_ID]:'string'
    }
}