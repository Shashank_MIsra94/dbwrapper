import * as db from "../Constants";

export class Make {}

Make.schema = {
    name: db.TABLE_MAKE,
    primaryKey: db.MAKE_ID,
    properties: {
        [db.MAKE_ID]:'string',
        [db.MAKE_CENTRAL_ID]:{ type: 'string', optional:true},
        [db.MAKE_NAME]:'string'
    }
};