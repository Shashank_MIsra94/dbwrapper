import * as db from "../Constants";

export class Model {
}

Model.schema = {
    name: db.TABLE_MODEL,
    primaryKey: db.MODEL_ID,
    properties: {
        [db.MODEL_ID]: 'string',
        [db.MODEL_CENTRAL_ID]: {type: 'string', optional: true},
        [db.MODEL_NAME]: 'string',
        [db.MODEL_MAKE_ID]: 'string',
        [db.MODEL_MAKE_NAME]: 'string',
        [db.MODEL_BODY_TYPE]: 'string',
        [db.MODEL_RANK]: 'int',
        [db.MODEL_PARENT_ID]: 'string',
        [db.MODEL_MMV_COLUMN_MAKE_MODEL]: 'string'
    }
};