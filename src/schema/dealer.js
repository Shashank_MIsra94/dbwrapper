import * as db from "../Constants";

export class Dealer{}
Dealer.schema = {
    name:db.TABLE_DEALER,
    primaryKey:db.DEALER_ID,
    properties:{
        [db.DEALER_ID]:'string',
        [db.DEALER_UCDID]:'string',
        [db.DEALER_NAME]:'string',
        [db.DEALER_USERNAME]:'string',
        [db.DEALER_EMAIL]:'string',
        [db.DEALER_MOBILE_SMS]:'string',
        [db.DEALER_CITY]:'string',
        [db.DEALER_PASSWORD]:{type:'string', optional:true},
        [db.DEALER_CITY]:{type:'string', optional:true},
        [db.DEALER_ORG]:'string'
    }
};