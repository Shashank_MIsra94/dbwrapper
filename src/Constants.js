/**
 *
 *  CREATED BY: SHASHANK
 *
 */


// TABLE CONSTANTS
export const TABLE_CITY = "city";
export const TABLE_CITY_SCHEMA_VERSION = 1;
export const TABLE_MAKE = "make";
export const TABLE_MAKE_SCHEMA_VERSION = 1;
export const TABLE_MODEL = "model";
export const TABLE_MODEL_SCHEMA_VERSION = 1;
export const TABLE_VERSION = "version";
export const TABLE_VERSION_SCHEMA_VERSION = 1;
export const TABLE_DEALER = "dealer";
export const TABLE_DEALER_SCHEMA_VERSION = 1;
export const TABLE_SHOWROOM = "showroom";
export const TABLE_SHOWROOM_SCHEMA_VERSION = 1;
export const TABLE_LEADS = "model";
export const TABLE_LEADS_SCHEMA_VERSION = 1;
export const TABLE_STOCKS = "stock";
export const TABLE_STOCKS_SCHEMA_VERSION = 1;

export const _ID = "_id";

// KEYS CITY TABLE
export const CITY_ID = "cityId";
export const CITY_CENTRAL_ID = "c_cityId";
export const CITY_NAME = "cityName";
export const CITY_STATE_ID = "stateId";
export const CITY_STATE_NAME = "stateName";
export const CITY_REGION_ID = "regionId";
export const CITY_ORDER_BY = "orderBy";

// KEYS MAKE TABLE
export const MAKE_ID = "makeId";
export const MAKE_CENTRAL_ID = "c_makeId";
export const MAKE_NAME = "makeName";

// KEYS MODEL TABLE
export const MODEL_ID = "modelId";
export const MODEL_CENTRAL_ID = "c_modelId";
export const MODEL_NAME = "modelName";
export const MODEL_MAKE_ID = "modelMakeId";
export const MODEL_MAKE_NAME = "modelMakeName";
export const MODEL_BODY_TYPE = "modelBodyType";
export const MODEL_RANK = "modelRank";
export const MODEL_PARENT_ID = "modelParentId";
export const MODEL_MMV_COLUMN_MAKE_MODEL = "modelMmvMakeModel";

// KEYS VERSION TABLE
export const VERSION_ID = "versionId";
export const VERSION_CENTRAL_ID = "versionCentralId";
export const VERSION_NAME = "versionName";
export const VERSION_CC = "versionCc";
export const VERSION_MODEL_ID = "versionModelid";
export const VERSION_MAKE_ID = "versionMakeId";
export const VERSION_FUEL_TYPE = "versionFuleType";
export const VERSION_TRANSMISSION_TYPE = "versionTransmissionType";
export const VERSION_MMV_MAKE_MODEL = "versionMmvMakeModel";
export const VERSION_MMV_MODEL_VERSION = "verisonMmvModelVersion";
export const VERSION_MMV_MAKE_MODEL_VERSION = "versionMmvMakeModelVersion";
export const VERSION_MAKE_NAME = "versionmakeName";
export const VERSION_MODEL_NAME = "versionModelName";
export const VERSION_MODEL_LAUNCH_YEAR = "versionModelLaunchYear";
export const VERSION_MODEL_END_YEAR = "versionModelEndYear";

// KEYS SHOWROOM TABLE
export const SHOWROOM_ID = "showroomId";
export const SHOWROOM_DEALER_ID = "showrromDealerId";
export const SHOWROOM_NAME = "showroomName";

// KEYS DEALER TABLE
export const DEALER_ID = "dealerId";
export const DEALER_UCDID = "dealerUcdid";
export const DEALER_NAME = "dealerName";
export const DEALER_USERNAME = "dealerUsername";
export const DEALER_EMAIL = "dealerEmail";
export const DEALER_MOBILE_SMS = "dealerMobileSms";
export const DEALER_PASSWORD = "dealerPassword";
export const DEALER_CITY = "dealerCity";
export const DEALER_ORG = "dealerOrg";

// KEYS STOCKS TABLE
export const STOCK_ID = "stockId";
export const STOCK_DEALER_PLATFORM = "stockDealerPlatform";
export const STOCK_BODY_TYPE = "stockBodyType";
export const STOCK_FINACNE_LIST = "stockFinanceList";
export const STOCK_CHANGE_TIME = "stockChangeTime";
export const STOCK_URL = "stockUrl";
export const STOCK_CAR_CERTIFICATION = "stockCarCertification";
export const STOCK_USER_NAME = "stockUserName";
export const STOCK_MAKE_NAME = "stockMakeName";
export const STOCK_MODEL_NAME = "stockModelName";
export const STOCK_VERSION_NAME = "stockVersionName";
export const STOCK_PARENT_MODEL_NAME = "stockParentModelName";
export const STOCK_MODEL_VERSION = "stockModelVersion";
export const STOCK_KMS = "stockKms";
export const STOCK_PRICE = "stockPrice";
export const STOCK_COLOR = "stockColor";
export const STOCK_MM = "stockMm";
export const STOCK_FUEL_TYPE = "stockFuelType";
export const STOCK_REG_NO = "stockRegNo";
export const STOCK_MODEL_YEAR = "stockModelYear";
export const STOCK_MOBILE = "stockMobile";
export const STOCK_EMAIL = "stockEmail";
export const STOCK_SHOWROOMID = "stockShowroomId";
export const STOCK_GAADIID = "stockGaadiId";
export const STOCK_CAR_ID = "stockCarId";
export const STOCK_CREATE_DATE = "stockCreateDate";
export const STOCK_SHARE_TEXT = "stockShareText";
export const STOCK_HEX_CODE = "stockHexCode";
export const STOCK_TRUST_MARK_CERTIFY = "stockTrustMarkCertify";
export const STOCK_ACTIVE = "stockActive";
export const STOCK_DOMAIN = "stockDomain";
export const STOCK_IMAGE_ICON = "stockImageIcon";
export const STOCK_AREA_OF_COVER = "stockAreaOfCover";
export const STOCK_PRICE_SORT = "stockPriceSort";
export const STOCK_OFFER_COUNT = "stockOfferCount";
export const STOCK_KM_SORT = "stockKmSort";
export const STOCK_INSPECTED_CAR = "stockInspectedCar";
export const STOCK_TOTAL_LEADS = "StockTotalLeads";
export const STOCK_TRANSMISSION = "stockTransmission";
export const STOCK_REG_MONTH = "stockRegMonth";
export const STOCK_REG_DAY = "stockRegDay";
export const STOCK_REG_YEAR = "stockRegyear";
export const STOCK_CAR_VERSION = "stockCarVersion";
export const STOCK_CNG_LPG_ENDORSEMENT = "stockCndLpgEndorsement";
export const STOCK_FLIPMENT = "stockFlipment";
export const STOCK_IMAGE = "stockImage";
export const STOCK_RC_URL = "stockRcUrl";
export const STOCK_RC_URL_2 = "stockRcUrl2";
export const STOCK_BOOKED = "stockBooked";
export const STOCK_IS_RSA = "stockIsRsa";
export const STOCK_MAKE_ID = "stockMakeId";
export const STOCK_MODEL_ID = "stockModelId";
export const STOCK_VERSION_ID = "stockVetrsionId";
export const STOCK_MMONTH = "stockMMonth";
export const STOCK_CHAHSIS_NUM = "stockChahisNum";
export const STOCK_RENEW_COUNT = "stockRenewCount";
export const STOCK_ENGINE_NUM = "stockEngineNum";
export const STOCK_IMAGES_ARRAY = "stockImagesArray";
export const STOCK_EMI_FINANCE = "stockEmiFinance";
export const STOCK_IS_CLASSIFIED = "stockIsClassified";
export const STOCK_IS_INSPECTED = "stockIsInspected";
export const STOCK_IS_FEATURED = "stockIsFeatured";
export const STOCK_PRICE_EDIT_MESSAGE = "stockPriceEditMessage";
export const STOCK_DEALER_PRICE = "stockealerPrice";
export const STOCK_INSPECTION_EXPIRY_DATE = "stockInspectionExpirydate";
export const STOCK_OWNER_TYPE = "stockOwnerType";
export const STOCK_UPDATE_TIME = "stockUpdateTime";
// extra fields for STOCK TABLE
export const STOCK_MAKE = "stockMake";
export const STOCK_TYPE = "stockType";

// KEYS LEADS TABLE
export const LEAD_COLUMN_ID = "leadsColumnId";
export const LEAD_TYPE = "leadType";
export const LEAD_DEALER_ID = "leadDealerId";
export const LEAD_RATING = "leadDealerRating";
export const LEAD_FOLLOW_DATE_ANDROID  ="leadFollowDateAndroid";
export const LEAD_CHANGE_TIME = "leadChangeTime";
export const LEAD_WALKIN_DATE = "leadWalkinDate";
export const LEAD_REMINDER_DATE = "leadReminderDate";
export const LEAD_CREATED_ON = "leadCreatedOn";
export const LEAD_CURRENT_TIMESTAMP = "leadCurrentTimestamp";
export const LEAD_REQUEST_CODE = "leadRequestCode";
export const LEAD_NOTIFICATION_TIME = "leadNotificationTime";
export const LEAD_MAKE = "leadmake";
export const LEAD_MAKE_ID = "leadmakeid";
export const LEAD_MODEL = "leadModel";
export const LEAD_COLUMN_VERSION = "leadColumnVersion";
export const LEAD_LMS_STATUS = "leadLmsStatus";
export const LEAD_ID = "leadId";
export const LEAD_DATETIME = "leadDatetime";
export const LEAD_NAME = "leadName";
export const LEAD_NUMBER = "leadNumber";
export const LEAD_BUDGET = "leadBudget";
export const LEAD_SOURCE = "leadSource";
export const LEAD_STATUS = "leadStatus";
export const LEAD_VERIFIED = "leadVerified";
export const LEAD_JSON_FORMAT = "leadJsonFormat";