import React from 'react';

export function getConfig() {
    return require("./configDev.json");
}

export function l() {
    if (getConfig().logging) {
        console.log(...arguments);
    }
}

export function paramsToBody(params) {
    if (!params || params.length < 1) {
        console.log("response : empty params");
        return null;
    }
    const body = new FormData();
    for (let k in params) {
        body.append(k, params[k]);
    }

    l("Params: ", JSON.stringify(body));
    return body;
}

export function merge(...parts) {
    let response = "";
    for (let p in parts) {
        if (p.trim().length > 0) {
            response = response + p + " ";
        }
    }
    return response;
}